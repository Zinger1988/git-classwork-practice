function getElementSelector(selector, action, func){

    selector.addEventListener(action, func);
}

function getUserData() {
    const name = prompt('Your name?');
    const age = prompt('Your age?');
    const email = prompt('Your email?');

    const dataObj = JSON.stringify({name, age, email});
    localStorage.setItem('user-data', dataObj);
}